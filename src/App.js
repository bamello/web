import React, { Component } from 'react';
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="pageContent">
        <div>
          <div className="intro full-height-panel">
            <div className="-wrapper">
              <div className="column-two-thirds">
                <h2 className="-heading">Bamello.</h2>
                <p>We always strive to do more, and to do important and meaningful things with the resources we have.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
